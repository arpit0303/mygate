package com.test.mygate.home;

import android.content.Context;
import android.graphics.Bitmap;

public interface HomeContract {

    interface View {

        Context getContext();
    }

    interface Presenter {
        void insertIntoDB(Bitmap bitmap);

        void getAllUsers();
    }
}
