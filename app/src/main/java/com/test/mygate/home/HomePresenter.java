package com.test.mygate.home;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.test.mygate.db.AppDataBase;
import com.test.mygate.db.User;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

public class HomePresenter implements HomeContract.Presenter {

    private HomeContract.View view;
    private Handler handler;

    private static final int RAND_MIN = 100000;
    private static final int RAND_MAX = 999999;
    static final String USER_KEY = "USER_KEY";

    private HashSet<Integer> hashSet = new HashSet<>();

    HomePresenter(HomeContract.View view, Handler handler) {
        this.view = view;
        this.handler = handler;
    }

    @Override
    public void insertIntoDB(final Bitmap bitmap) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                User user = new User();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 0 /* Ignored for PNGs */, byteArrayOutputStream);
                byte[] bitmapdata = byteArrayOutputStream.toByteArray();

                user.setUserPic(bitmapdata);
                user.setUserName("User " + user.getUid());
                int code = generateRandomNumber();
                user.setUserCode(code);
                AppDataBase.getInstance(view.getContext()).userDao().insertAll(user);

                ArrayList<User> userList = (ArrayList<User>) AppDataBase.getInstance(view.getContext()).userDao().getAll();
                for (User mUser : userList) {
                    hashSet.add(mUser.getUserCode());
                }

                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(USER_KEY, userList);

                Message msg = handler.obtainMessage();
                msg.setData(bundle);
                handler.sendMessage(msg);
            }
        }).start();
    }

    @Override
    public void getAllUsers() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<User> userList = (ArrayList<User>) AppDataBase.getInstance(view.getContext()).userDao().getAll();
                for (User user : userList) {
                    hashSet.add(user.getUserCode());
                }

                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(USER_KEY, userList);

                Message msg = handler.obtainMessage();
                msg.setData(bundle);
                handler.sendMessage(msg);
            }
        }).start();
    }

    private int generateRandomNumber() {
        Random rand = new Random();
        int code = rand.nextInt(RAND_MAX - RAND_MIN) + RAND_MIN;

        if (hashSet.contains(code)) {
            generateRandomNumber();
            return 0;
        } else {
            hashSet.add(code);
            return code;
        }
    }
}
