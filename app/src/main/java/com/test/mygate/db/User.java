package com.test.mygate.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Arpit on 02/02/19.
 */

@Entity
public class User  implements Parcelable{

    @PrimaryKey(autoGenerate = true)
    private int uid = 0;

    @ColumnInfo(name = "pic")
    private byte[] userPic;

    @ColumnInfo(name = "user_name")
    private String userName;

    @ColumnInfo(name = "user_code")
    private int userCode;

    public byte[] getUserPic() {
        return userPic;
    }

    public void setUserPic(byte[] userPic) {
        this.userPic = userPic;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserCode() {
        return userCode;
    }

    public void setUserCode(int userCode) {
        this.userCode = userCode;
    }

    public User() {

    }

    private User(Parcel in) {
        uid = in.readInt();
        userPic = (byte[]) in.readValue(Bitmap.class.getClassLoader());
        userName = in.readString();
        userCode = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(uid);
        dest.writeValue(userPic);
        dest.writeString(userName);
        dest.writeInt(userCode);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
